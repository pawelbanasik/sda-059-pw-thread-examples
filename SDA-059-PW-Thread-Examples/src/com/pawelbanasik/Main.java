package com.pawelbanasik;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		// Cz�� pierwsza - teoretyczna
		
		// zeby napisac watek to klasa albo musi dziedziczyc thread albo implementowac interface Runnable
		
		// tworzy sobie pierwszy watek
//		Thread t = new Thread(new MyThreadExample(11), "watek pierwszy");
		
		// metoda start() z klasy thread uruchamia metode run() interface'u runnable t.start();
//		new Thread(new OtherThread(), "watek drugi").start();

		
		// tak sie ustawia priorytet watku
//		t.setPriority(Thread.MAX_PRIORITY);

		// dodal sobie kolejny watek w celu pokazania jak to dziala
//		Thread t3 = new Thread(() -> System.out.println("watek trzeci"));
//		t3.start();

		// ustawienie watku daemon
		// A daemon thread is a thread that is considered doing some tasks in the background like handling requests or various chronjobs that can exist in an application.
		// When your program only have damon threads remaining it will exit. That's because usually these threads work together with normal threads and provide background handling of events.
//		t3.setDaemon(true);
		
		// wpisal zeby pokazac jak to kolejno dziala
//		System.out.println("Koniec");

		
		// sprawdzamy ile mamy watkow w obrebie naszej aplikacji
//		System.out.println("Ilosc watkow: " + Thread.activeCount()); //
		

		
		// Czesc druga - omawianie BigInteger
//		BigInteger b = new BigInteger("100000000000001");
		
		// w taki sposob zapisujemy zeby dodac liczbe 10 w postaci BigInteger
//		b = b.add(BigInteger.TEN);
		
		// drukowanie
//		System.out.println(b.toString());

		
		// Sprawdzenie jak szybko wykonuje obliczenia czy dana liczba jest liczba pierwsza
		// akurat tu wykorzystuje typy proste
//		MathHelper mh = new MathHelper();
//		long start = System.currentTimeMillis();
//		System.out.println("Liczba 858599509 jest liczba pierwsza: " + mh.isPrimeWhile(858599509));
//		long stop =  System.currentTimeMillis();
//		System.out.println("Wykonanie zajelo: " + (stop - start) + "ms");

		
		// BigInteger to nie typ prosty wiec uzywa sie metod zeby na nich dzialac
		// trzeba uzywac valueOf bo nie bedzie dzialac
//		BigInteger bi1 = BigInteger.valueOf(123);
//		BigInteger bi2 = BigInteger.valueOf(555);
//		System.out.println(bi1.compareTo(bi2));

		// BARDZO WA�NE DLA BigInteger
		// -1 liczba jest mniejsza
		// 0 liczby sa takie same
		// 1 liczba jest wieksza

		
		// Zadania z BigInteger
		// Zapis dzialania dla BigInteger (konieczne sa metody)
//		 BigInteger x = BigInteger.valueOf(3);
		
		 // Zadanie 1: 2 + x
//		 BigInteger d1 = new BigInteger("2");
//		 d1 = d1.add(x);
//		 System.out.println("2 + x = " + d1.toString());
		
		 //Zadanie 2: 2x + 5
//		 BigInteger d2 =
//		 (BigInteger.valueOf(2).multiply(x)).add(BigInteger.valueOf(5));
//		 System.out.println("2x + 5 = " + d2);
		
		 // Zadanie 3: 4x do kwadratu + 5x + 6
//		 BigInteger d3 =
//		 (BigInteger.valueOf(4).multiply(x).multiply(x)).add((BigInteger.valueOf(5)).multiply(x))
//		 .add(BigInteger.valueOf(6));
//		 System.out.println("4 x do kwadratu + 5x + 6 = " + d3);
		
		 // Zadanie 4: 7/2 + 3
		 // nie jest wynikiem 6,5 bo obcina
//		 BigInteger d4 =
//		 (BigInteger.valueOf(7).divide(BigInteger.valueOf(2))).add(BigInteger.valueOf(3));
//		 System.out.println("7 / 2 + 3 = " + d4);
		
		 // Zadanie 5: 9 - 15 % x
//		 BigInteger d5 =
//		 BigInteger.valueOf(9).subtract((BigInteger.valueOf(15)).mod(x));
//		 System.out.println("9 - 15 % x = " + d5);
		
		 // Zadanie 6: (x % 3) * (x % 4) + 5
//		 BigInteger d6 =
//		 (x.mod(BigInteger.valueOf(3))).multiply((x).mod(BigInteger.valueOf(4)))
//		 .add(BigInteger.valueOf(5));
//		 System.out.println("(x % 3) * (x % 4) + 5 = " + d6);
		
		 // Zadanie 7: [(x do trzeciej + (3x/x do kwadratu) + 7 - x do kwadratu ] % x
//		 BigInteger d7 =
//		 (((x.multiply(x).multiply(x))).add((BigInteger.valueOf(3).multiply(x))).divide((x).multiply(x))
//		 .add((BigInteger.valueOf(7)).subtract((x).multiply(x)))).mod(x);
//		 System.out.println(d7);

		// sprawdzamy jeszcze raz czas wykonania tym razem na BigInteger
//		 MathHelper mh = new MathHelper();
//		 long start = System.currentTimeMillis();
//		 System.out.println("Liczba 49979693 jest licza pierwsza: " + mh.isBigPrime(BigInteger.valueOf(49979693)));
//		 long stop = System.currentTimeMillis();
//		 System.out.println("Wywolanie zajelo: " + (stop - start) + " ms");
		
		// znowu sprawdzamy tym razem isPrimeWhile
//		MathHelper mh = new MathHelper();
//		long start = System.currentTimeMillis();
//		System.out.println("Liczba 49979693 jest pierwsza: " + mh.isPrimeWhile(49979693));
//		long stop = System.currentTimeMillis();
//		System.out.println("Wywolanie zajelo: " + (stop - start) + " ms");

		List<String> input = Arrays.asList("49979693", "198491329", "314606891", "393342743", "553105243", "715225739",
				"899809343", "982451000653");
		

		// piszemy kod ktory dla kazdego elementu sprawdzi czas wykonania sprawdzenia poszczegolnych liczb na liscie czy sa pierwsze wykorzystujac typ prosty
//		MathHelper mh = new MathHelper();
//		for (int i = 0; i < input.size(); i++) {
//			long start = System.currentTimeMillis();
//			System.out.println("Liczba jest pierwsza: " + mh.isPrimeWhile(Long.valueOf(input.get(i))));
//			long stop = System.currentTimeMillis();
//			System.out.println("Wywolanie zajelo: " + (stop - start) + " ms");
//		 }
	
		
		// piszemy kod ktory dla kazdego elementu sprawdzi czas wykonania sprawdzenia poszczegolnych liczb na liscie czy sa pierwsze wykorzystujac BigInteger
		MathHelper mh = new MathHelper();
		for(int i = 0; i < input.size(); i += 2) {
			final String in = input.get(i);
			new Thread(() -> System.out.println(in + " = " + mh.isBigPrime(new BigInteger(in)))).start();
			if(input.size() % 2 == 0 || i != input.size()) {
				final String in2 = input.get(i+1);
				new Thread(() -> System.out.println(in2 + " = " + mh.isBigPrime(new BigInteger(in2)))).start();
			}
		}
	}
}

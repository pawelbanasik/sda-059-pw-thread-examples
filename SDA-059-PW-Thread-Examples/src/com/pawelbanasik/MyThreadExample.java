package com.pawelbanasik;

public class MyThreadExample extends Thread {

	private int number = 0;

	public MyThreadExample() {

	}

	public MyThreadExample(int number) {
		this.number = number;
	}

	// klasa Thread implementuje interface Runnable nadpisuje metode run()
	@Override
	public void run(){
		System.out.println("Twoja liczba to: " + number);
	}
	
}

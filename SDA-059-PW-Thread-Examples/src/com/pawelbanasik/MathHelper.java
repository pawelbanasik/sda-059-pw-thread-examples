package com.pawelbanasik;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class MathHelper {

	// sprawdzenie czy wprowadzona jako parametr liczba jest pierwsza przy pomocy for
	public boolean isPrime(long n) {
		for (int i = 2; i < Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;

	}
	
	// sprawdzenie czy wprowadzona jako parametr liczba jest pierwsza przy pomocy while
	public static boolean isPrimeWhile(long n) {
		int i = 2;
		while (i < n) {
			if (n % i == 0) {
				return false;
			}
			i++;
		}
		return true;

	}

	public static boolean isBigPrime(BigInteger n) {
		// zainicjowane i na 2
		BigInteger i = BigInteger.valueOf(2);

		// zupelnie inny zapis na i < n dla BigIntegers
		while (i.compareTo(n) == -1) {
			if (n.mod(i).compareTo(BigInteger.ZERO) == 0) {
				return false;
			}
		// zupelnie inny zapis na i = i + 1 dla BigIntegers
			i = i.add(BigInteger.ONE); 
		}
		return true;
	}
}
